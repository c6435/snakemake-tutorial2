FROM condaforge/mambaforge
ENV SHELL /bin/bash 
ADD singularity.yml .
ADD snakemake.yml . 
RUN mamba env create -n snakemake-tutorial -f snakemake.yml
RUN mamba env create -n singularity -f singularity.yml

